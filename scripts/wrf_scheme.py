import numpy as np
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
from matplotlib import cm
from cartopy.feature import NaturalEarthFeature
from netCDF4 import Dataset
from datetime import datetime
from dateutil import tz
from wrf import to_np, getvar, CoordPair, vertcross,ALL_TIMES, extract_times,latlon_coords,get_cartopy
import os
import glob
import moviepy.editor as mpy
import cartopy.crs as ccrs
import cartopy.feature as cfeature


filename1 = "forecast_ysm.nc"
ncfile1 = Dataset(filename1)

z1 = getvar(ncfile1, "z")
time1 = extract_times(ncfile1, ALL_TIMES, method="cat")
wspd1 =  getvar(ncfile1, "uvmet_wspd_wdir", units="kt")[0,:]
ua1 = np.array(ncfile1.variables['U'][:,:,:,0:73])
va1 = np.array(ncfile1.variables['V'][:,:,0:60,:])
wa1 = np.array(ncfile1.variables['W'][:,0:29,:,:])
wind1 = np.sqrt(ua1**2 + va1**2 + wa1**2)
qvapor1 = np.array(ncfile1.variables['QVAPOR'][::])
temps1 = np.array(ncfile1.variables['T2'][::])
temps1 = temps1 - 273.15
temps1 = ((temps1 * (9./5.)) + 32.)
temp1 = getvar(ncfile1, "tk")

filename2 = "forecast_asm.nc"
ncfile2 = Dataset(filename2)

z2 = getvar(ncfile2, "z")
time2 = extract_times(ncfile2, ALL_TIMES, method="cat")
wspd2 =  getvar(ncfile2, "uvmet_wspd_wdir", units="kt")[0,:]
ua2 = np.array(ncfile2.variables['U'][:,:,:,0:73])
va2 = np.array(ncfile2.variables['V'][:,:,0:60,:])
wa2 = np.array(ncfile2.variables['W'][:,0:29,:,:])
wind2 = np.sqrt(ua2**2 + va2**2 + wa2**2)
qvapor2 = np.array(ncfile2.variables['QVAPOR'][::])
temps2 = np.array(ncfile2.variables['T2'][::])
temps2 = temps2 - 273.15
temps2 = ((temps2 * (9./5.)) + 32.)
temp2 = getvar(ncfile2, "tk")

filename3 = "forecast_nopbl.nc"
ncfile3 = Dataset(filename3)

z3 = getvar(ncfile3, "z")
time3 = extract_times(ncfile3, ALL_TIMES, method="cat")
wspd3 =  getvar(ncfile3, "uvmet_wspd_wdir", units="kt")[0,:]
ua3 = np.array(ncfile3.variables['U'][:,:,:,0:73])
va3 = np.array(ncfile3.variables['V'][:,:,0:60,:])
wa3 = np.array(ncfile3.variables['W'][:,0:29,:,:])
wind3 = np.sqrt(ua3**2 + va3**2 + wa3**2)
qvapor3 = np.array(ncfile3.variables['QVAPOR'][::])
temps3 = np.array(ncfile3.variables['T2'][::])
temps3 = temps3 - 273.15
temps3 = ((temps3 * (9./5.)) + 32.)
temp3 = getvar(ncfile3, "tk")

time_string=[]
for i in range(41):
    string = str(time1[i])
    string = string[0:13]
    time_string.append(string)
 
    
EST = []
from_zone = tz.tzutc()
to_zone = tz.tzlocal()
counter = 0
for i in range(41):
    utc = datetime.strptime(time_string[i], '%Y-%m-%dT%H')
    utc = utc.replace(tzinfo=from_zone)
    eastern = utc.astimezone(to_zone)
    string = str(eastern)
    EST.append(string[6:13])


start_point = CoordPair(lat=20, lon=-76.0)
end_point = CoordPair(lat=50, lon=-76.0)
wind_levels = np.arange(0,70.1,0.1)
wspd1 = vertcross(wspd1, z1, wrfin=ncfile1, start_point=start_point,end_point=end_point, latlon=True, meta=True)
wspd2 = vertcross(wspd2, z2, wrfin=ncfile2, start_point=start_point,end_point=end_point, latlon=True, meta=True)
wspd3 = vertcross(wspd3, z3, wrfin=ncfile3, start_point=start_point,end_point=end_point, latlon=True, meta=True)
for i in range(41):
    wspd_cross1 = vertcross(wind1[i,:,:,:], z1, wrfin=ncfile1, start_point=start_point,end_point=end_point, latlon=True, meta=True)
    wspd_cross2 = vertcross(wind2[i,:,:,:], z2, wrfin=ncfile2, start_point=start_point,end_point=end_point, latlon=True, meta=True)
    wspd_cross3 = vertcross(wind3[i,:,:,:], z3, wrfin=ncfile3, start_point=start_point,end_point=end_point, latlon=True, meta=True)
    fig = plt.figure(figsize=(16,8))
    ax1 = fig.add_subplot(1,3,1)
    wspd_contours = ax1.contourf(to_np(wspd_cross1),wind_levels,cmap=get_cmap("jet"),extend="both")
    coord_pairs = to_np(wspd1.coords["xy_loc"])
    x_ticks = np.arange(coord_pairs.shape[0])
    x_labels = [pair.latlon_str(fmt="{:.0f}")for pair in to_np(coord_pairs)]
    ax1.set_xticks(x_ticks[::5])
    ax1.set_xticklabels(x_labels[::5],fontsize=12)
    vert_vals = to_np(wspd1.coords["vertical"])
    v_ticks = np.arange(vert_vals.shape[0])
    ax1.set_yticks(v_ticks[::20])
    ax1.set_yticklabels(vert_vals[::20], fontsize=8,rotation=90)
    # Set the x-axis and  y-axis labels
    ax1.set_xlabel("Latitude " + r'($\degree$N)', fontsize=12)
    ax1.set_ylabel("Height (m)", fontsize=12)
    ax1.set_title("PBL Scheme: YSU")
    plt.colorbar(wspd_contours, ax=ax1,orientation="horizontal",ticks=wind_levels[::100])
    ax2 = fig.add_subplot(1,3,2)
    wspd_contours = ax2.contourf(to_np(wspd_cross2),wind_levels,cmap=get_cmap("jet"),extend="both")
    coord_pairs = to_np(wspd2.coords["xy_loc"])
    x_ticks = np.arange(coord_pairs.shape[0])
    x_labels = [pair.latlon_str(fmt="{:.0f}")for pair in to_np(coord_pairs)]
    ax2.set_xticks(x_ticks[::5])
    ax2.set_xticklabels(x_labels[::5],fontsize=12)
    vert_vals = to_np(wspd2.coords["vertical"])
    v_ticks = np.arange(vert_vals.shape[0])
    ax2.set_yticks(v_ticks[::20])
    ax2.set_yticklabels(vert_vals[::20], fontsize=8,rotation=90)
    # Set the x-axis and  y-axis labels
    ax2.set_xlabel("Latitude " + r'($\degree$N)', fontsize=12)
    ax2.set_ylabel("Height (m)", fontsize=12)
    ax2.set_title("PBL Scheme: ACM2")
    plt.colorbar(wspd_contours, ax=ax2,orientation="horizontal",ticks=wind_levels[::100])
    ax3 = fig.add_subplot(1,3,3)
    wspd_contours = ax3.contourf(to_np(wspd_cross3),wind_levels,cmap=get_cmap("jet"),extend="both")
    coord_pairs = to_np(wspd3.coords["xy_loc"])
    x_ticks = np.arange(coord_pairs.shape[0])
    x_labels = [pair.latlon_str(fmt="{:.0f}")for pair in to_np(coord_pairs)]
    ax3.set_xticks(x_ticks[::5])
    ax3.set_xticklabels(x_labels[::5],fontsize=12)
    vert_vals = to_np(wspd3.coords["vertical"])
    v_ticks = np.arange(vert_vals.shape[0])
    ax3.set_yticks(v_ticks[::20])
    ax3.set_yticklabels(vert_vals[::20], fontsize=8,rotation=90)
    # Set the x-axis and  y-axis labels
    ax3.set_xlabel("Latitude " + r'($\degree$N)', fontsize=12)
    ax3.set_ylabel("Height (m)", fontsize=12)
    ax3.set_title("PBL Scheme: None")
    plt.colorbar(wspd_contours, ax=ax3,orientation="horizontal",ticks=wind_levels[::100])
    plt.suptitle("Vertical Cross Section of Wind Speed " + r'($\frac{m}{s}$)' + "\nDate: " + EST[i]+" EST")
#    plt.show()
    plt.savefig("forecast_"+str(i)+".jpeg", dpi=200,bbox_inches = 'tight')
    plt.close()
    
mp4_name = 'wind_cross_section'
fps=3
file_list1 = glob.glob('*.jpeg')
list.sort(file_list1, key=lambda x: int(x.split('_')[1].split('.jpeg')[0]))
clip = mpy.ImageSequenceClip(file_list1, fps=fps)
clip.write_gif('{}.gif'.format(mp4_name), fps=fps)

dir_name = "C:\Users\Colin\Desktop\CFD\lab4"
test = os.listdir(dir_name)
for item in test:
    if item.endswith(".jpeg"):
        os.remove(os.path.join(dir_name, item))


####################################
####################################
water_levels = np.arange(0.0,0.02,0.0001)
#wspd1 = vertcross(wspd1, z1, wrfin=ncfile, start_point=start_point,end_point=end_point, latlon=True, meta=True)
#wspd2 = vertcross(wspd2, z2, wrfin=ncfile, start_point=start_point,end_point=end_point, latlon=True, meta=True)
#wspd3 = vertcross(wspd3, z3, wrfin=ncfile, start_point=start_point,end_point=end_point, latlon=True, meta=True)
for i in range(41):
    wspd_cross1 = vertcross(qvapor1[i,:,:,:], z1[:,:,:], wrfin=ncfile1, start_point=start_point,end_point=end_point, latlon=True, meta=True)
    wspd_cross2 = vertcross(qvapor2[i,:,:,:], z2[:,:,:], wrfin=ncfile2, start_point=start_point,end_point=end_point, latlon=True, meta=True)
    wspd_cross3 = vertcross(qvapor3[i,:,:,:], z3[:,:,:], wrfin=ncfile3, start_point=start_point,end_point=end_point, latlon=True, meta=True)
    fig = plt.figure(figsize=(12,8))
    ax1 = fig.add_subplot(1,3,1)
    wspd_contours = ax1.contourf(to_np(wspd_cross1),water_levels,cmap=get_cmap("RdYlBu"),extend="both")
    coord_pairs = to_np(wspd1.coords["xy_loc"])
    x_ticks = np.arange(coord_pairs.shape[0])
    x_labels = [pair.latlon_str(fmt="{:.0f}")for pair in to_np(coord_pairs)]
    ax1.set_xticks(x_ticks[::5])
    ax1.set_xticklabels(x_labels[::5],fontsize=12)
    vert_vals = to_np(wspd1[:,:].coords["vertical"])
    v_ticks = np.arange(vert_vals.shape[0])
    ax1.set_yticks(v_ticks[::20])
    ax1.set_yticklabels(vert_vals[::20], fontsize=8,rotation=90)
    # Set the x-axis and  y-axis labels
    ax1.set_xlabel("Latitude " + r'($\degree$N)', fontsize=12)
    ax1.set_ylabel("Height (m)", fontsize=12)
    ax1.set_title("PBL Scheme: YSU")
    plt.colorbar(wspd_contours, ax=ax1,orientation="horizontal",ticks=water_levels[::50])
    ax2 = fig.add_subplot(1,3,2)
    wspd_contours = ax2.contourf(to_np(wspd_cross2),water_levels,cmap=get_cmap("RdYlBu"),extend="both")
    coord_pairs = to_np(wspd2.coords["xy_loc"])
    x_ticks = np.arange(coord_pairs.shape[0])
    x_labels = [pair.latlon_str(fmt="{:.0f}")for pair in to_np(coord_pairs)]
    ax2.set_xticks(x_ticks[::5])
    ax2.set_xticklabels(x_labels[::5],fontsize=12)
    vert_vals = to_np(wspd2[:,:].coords["vertical"])
    v_ticks = np.arange(vert_vals.shape[0])
    ax2.set_yticks(v_ticks[::20])
    ax2.set_yticklabels(vert_vals[::20], fontsize=8,rotation=90)
    # Set the x-axis and  y-axis labels
    ax2.set_xlabel("Latitude " + r'($\degree$N)', fontsize=12)
    ax2.set_ylabel("Height (m)", fontsize=12)
    ax2.set_title("PBL Scheme: ACM2")
    plt.colorbar(wspd_contours, ax=ax2,orientation="horizontal",ticks=water_levels[::50])
    ax3 = fig.add_subplot(1,3,3)
    wspd_contours = ax3.contourf(to_np(wspd_cross3),water_levels,cmap=get_cmap("RdYlBu"),extend="both")
    coord_pairs = to_np(wspd3.coords["xy_loc"])
    x_ticks = np.arange(coord_pairs.shape[0])
    x_labels = [pair.latlon_str(fmt="{:.0f}")for pair in to_np(coord_pairs)]
    ax3.set_xticks(x_ticks[::5])
    ax3.set_xticklabels(x_labels[::5],fontsize=12)
    vert_vals = to_np(wspd3[:,:].coords["vertical"])
    v_ticks = np.arange(vert_vals.shape[0])
    ax3.set_yticks(v_ticks[::20])
    ax3.set_yticklabels(vert_vals[::20], fontsize=8,rotation=90)
    # Set the x-axis and  y-axis labels
    ax3.set_xlabel("Latitude " + r'($\degree$N)', fontsize=12)
    ax3.set_ylabel("Height (m)", fontsize=12)
    ax3.set_title("PBL Scheme: None")
    plt.colorbar(wspd_contours, ax=ax3,orientation="horizontal",ticks=water_levels[::50])
    plt.suptitle("Vertical Cross Section of Specific Humidity " + r'($\frac{g}{kg}$)' + "\nDate: " + EST[i]+" EST")
#    plt.show()
    plt.savefig("forecast_"+str(i)+".jpeg", dpi=200,bbox_inches = 'tight')
    plt.close()
    
mp4_name = 'q_cross_section'
fps=3
file_list1 = glob.glob('*.jpeg')
list.sort(file_list1, key=lambda x: int(x.split('_')[1].split('.jpeg')[0]))
clip = mpy.ImageSequenceClip(file_list1, fps=fps)
clip.write_gif('{}.gif'.format(mp4_name), fps=fps)

dir_name = "C:\Users\Colin\Desktop\CFD\lab4"
test = os.listdir(dir_name)
for item in test:
    if item.endswith(".jpeg"):
        os.remove(os.path.join(dir_name, item))

################################
################################
temp_levels = np.arange(-50,101,1)
states_provinces = cfeature.NaturalEarthFeature(category='cultural',name='admin_1_states_provinces_lines',scale='50m',facecolor='none')
for i in range(41):
    fig = plt.figure(figsize=(20,8))
    lats, lons = latlon_coords(temp1)
    cart_proj = get_cartopy(temp1)
    ax1 = fig.add_subplot(1,3,1, projection=cart_proj)
    ax1.coastlines(resolution='50m', linewidth=0.5)
    ax1.add_feature(states_provinces, edgecolor='k', linewidth=0.5)
    ax1.add_feature(cfeature.BORDERS, edgecolor='k', linewidth=0.5)
    ax1.set_extent([-114,-55,20,64])####([-80,-70,40,45])
    contours = ax1.contourf(to_np(lons), to_np(lats), temps1[i,:,:], temp_levels, transform=ccrs.PlateCarree(), cmap=get_cmap('jet'))
    ax1.set_title("PBL Scheme: YSU",fontsize=15)
    plt.colorbar(contours,orientation="horizontal", label="Temperature(F)")
    lats, lons = latlon_coords(temp2)
    cart_proj = get_cartopy(temp2)
    ax2 = fig.add_subplot(1,3,2,projection=cart_proj)
    ax2.coastlines(resolution='50m', linewidth=0.5)
    ax2.add_feature(states_provinces, edgecolor='k', linewidth=0.5)
    ax2.add_feature(cfeature.BORDERS, edgecolor='k', linewidth=0.5)
    ax2.set_extent([-114,-55,20,64])####([-80,-70,40,45])
    contours = ax2.contourf(to_np(lons), to_np(lats), temps2[i,:,:], temp_levels, transform=ccrs.PlateCarree(), cmap=get_cmap('jet'))
    ax2.set_title("PBL Scheme: ASM2",fontsize=15)
    plt.colorbar(contours,orientation="horizontal", label="Temperature(F)")
    lats, lons = latlon_coords(temp3)
    cart_proj = get_cartopy(temp3)
    ax3 = fig.add_subplot(1,3,3,projection=cart_proj)
    ax3.coastlines(resolution='50m', linewidth=0.5)
    ax3.add_feature(states_provinces, edgecolor='k', linewidth=0.5)
    ax3.add_feature(cfeature.BORDERS, edgecolor='k', linewidth=0.5)
    ax3.set_extent([-114,-55,20,64])####([-80,-70,40,45])
    contours = ax3.contourf(to_np(lons), to_np(lats), temps3[i,:,:], temp_levels, transform=ccrs.PlateCarree(), cmap=get_cmap('jet'))
    ax3.set_title("PBL Scheme: None",fontsize=15)
    plt.suptitle("Temperature Forecast\nDate: " + EST[i]+" EST",fontsize=30)
    plt.colorbar(contours,orientation="horizontal", label="Temperature(F)")
    plt.savefig("forecast_"+str(i)+".jpeg", dpi=200,bbox_inches = 'tight')
#    plt.show()
    plt.close()


mp4_name = 'temp_forecast'
fps=3
file_list1 = glob.glob('*.jpeg')
list.sort(file_list1, key=lambda x: int(x.split('_')[1].split('.jpeg')[0]))
clip = mpy.ImageSequenceClip(file_list1, fps=fps)
clip.write_gif('{}.gif'.format(mp4_name), fps=fps)

dir_name = "C:\Users\Colin\Desktop\CFD\lab4"
test = os.listdir(dir_name)
for item in test:
    if item.endswith(".jpeg"):
        os.remove(os.path.join(dir_name, item))
