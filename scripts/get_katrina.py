import argparse
from datetime import datetime,date, timedelta
import os
import numpy as N
import sys

wgetTemplate="wget -O %s %s"
oDir="/home/cpe28/cfd/data/"
urlTemplate="ftp://ftp.ucar.edu/vapor/data/Katrina/wrfout_d02_2005-08-%s_%s.gz"
os.chdir(oDir)
for i in range(29,32,1):
    day = str(i)
    for j in range(24):
        if j < 10:
            time = str(0) + str(j)
            file_name = oDir + "2005_08-"+str(i)+"_0"+str(j)+".gz"
#            print time
        else:
            time = str(j)
            file_name = oDir + "2005_08-" + str(i) + "_" + str(j)+".gz"
#        print file_name
        url = urlTemplate % (day, time)
#        print url
#        sys.exit()
        wgetCmmnd = wgetTemplate % (file_name,url)
        os.system("echo " + wgetCmmnd)
        os.system(wgetCmmnd)
