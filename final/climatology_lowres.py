from scipy import interpolate
import numpy as np
import netCDF4 as NC

CMD_MV = 'mv %s %s'
high_res_dirs = ['BNU', 'CCSM4','GFDL','MIROC','MRI']
low_res_dirs = ['access1-0', 'access1-3', 'cmcc', 'CNRM', 'csiro', 'EC-EARTH', 'FGOALS', 'GISS', 'HADLEY']
#low_res_dirs = ['cmcc', 'CNRM', 'csiro', 'EC-EARTH', 'FGOALS', 'GISS', 'HADLEY']
variables = ['evspsbl', 'evspsblsoi', 'evspsblveg', 'pr']

home_dir = '/data2/tra38/shared_projects/cpe28/CFD/low-res/%s/'

file_name = "p-e_%s_rcp85.nc"

def interp(data,lat, lon, lat_min, lat_max, lon_min, lon_max, lat_rank, lon_rank):

   lath = np.linspace(lat_min, lat_max, lat_rank)
   lonh = np.linspace(lon_min, lon_max, lon_rank)
   f = interpolate.interp2d(lon,lat, data, kind='linear')
   newdata = f(lonh,lath)

   return newdata, lath, lonh

low_res_data = np.empty([108,90,180])
counter = 0
for i in range(9):
    data = NC.Dataset(file_name % (low_res_dirs[i]))
    p_e = np.array(data.variables["p_e"][::])
    lat = np.array(data.variables["lat"][:])
    lon = np.array(data.variables["lon"][:])
    
    p_e[p_e<-99] = np.nan    

    climatology = np.empty([12,len(lat),len(lon)])
    for j in range(12):
        climatology[i,:,:] = np.nanmean(p_e[i::12,:,:],axis=0)
    
    re_gridded = np.empty([12,90,180])
    for j in range(12):
        re_gridded[i,:,:],lat_new,lon_new = interp(climatology[i,:,:],lat,lon,np.min(lat),np.max(lat),np.min(lon),np.max(lon),90,180)
    
    low_res_data[counter:(counter+12),:,:] = re_gridded
    counter = counter + 12

low_res_climo = np.empty([12,90,180])
for i in range(12):
    low_res_climo[i,:,:] = np.nanmean(low_res_data[i::12,:,:],axis=0)

nc_dataset = NC.Dataset('low_res_climo.nc', 'w', format='NETCDF4_CLASSIC')
time = nc_dataset.createDimension('time', len(low_res_climo[:,0,0]))
lat = nc_dataset.createDimension('lat', len(lat_new))
lon = nc_dataset.createDimension('lon', len(lon_new))
times = nc_dataset.createVariable('time', np.float, ('time',))
lats = nc_dataset.createVariable('lat', np.float, ('lat',))
lons = nc_dataset.createVariable('lon', np.float, ('lon',))
low_res = nc_dataset.createVariable('low_res', np.float, ('time', 'lat', 'lon'))
import time
nc_dataset.history = 'Created ' + time.ctime(time.time())
lats.units = 'degree_north'
lons.units = 'degree_east'
lats[:] = lat_new
lons[:] = lon_new
low_res[:] = low_res_climo
nc_dataset.close() 
    
    
    
high_res_data = np.empty([60,90,180])
counter = 0
for i in range(5):
    data = NC.Dataset(file_name % (high_res_dirs[i]))
    p_e = np.array(data.variables["p_e"][::])
    lat = np.array(data.variables["lat"][:])
    lon = np.array(data.variables["lon"][:])
    
    p_e[p_e<-99] = np.nan
    
    climatology = np.empty([12,len(lat),len(lon)])
    for j in range(12):
        climatology[i,:,:] = np.nanmean(p_e[i::12,:,:],axis=0)
    
    re_gridded = np.empty([12,90,180])
    for j in range(12):
        re_gridded[i,:,:],lat_new,lon_new = interp(climatology[i,:,:],lat,lon,np.min(lat),np.max(lat),np.min(lon),np.max(lon),90,180)
    
    high_res_data[counter:(counter+12),:,:] = re_gridded
    counter = counter + 12

high_res_climo = np.empty([12,90,180])
for i in range(12):
    high_res_climo[i,:,:] = np.nanmean(high_res_data[i::12,:,:],axis=0)

nc_dataset = NC.Dataset('high_res_climo.nc', 'w', format='NETCDF4_CLASSIC')
time = nc_dataset.createDimension('time', len(high_res_climo[:,0,0]))
lat = nc_dataset.createDimension('lat', len(lat_new))
lon = nc_dataset.createDimension('lon', len(lon_new))
times = nc_dataset.createVariable('time', np.float, ('time',))
lats = nc_dataset.createVariable('lat', np.float, ('lat',))
lons = nc_dataset.createVariable('lon', np.float, ('lon',))
high_res = nc_dataset.createVariable('high_res', np.float, ('time', 'lat', 'lon'))
import time
nc_dataset.history = 'Created ' + time.ctime(time.time())
lats.units = 'degree_north'
lons.units = 'degree_east'
lats[:] = lat_new
lons[:] = lon_new
high_res[:] = high_res_climo
nc_dataset.close() 
    
