import numpy as np
import netCDF4 as NC

CMD_MV = 'mv %s %s'
high_res_dirs = ['BNU', 'CCSM4','GFDL','MIROC','MRI']
low_res_dirs = ['access1-0', 'access1-3', 'cmcc', 'CNRM', 'csiro', 'EC-EARTH', 'FGOALS', 'GISS', 'HADLEY']
#low_res_dirs = ['cmcc', 'CNRM', 'csiro', 'EC-EARTH', 'FGOALS', 'GISS', 'HADLEY']
variables = ['evspsbl', 'evspsblsoi', 'evspsblveg', 'pr']

home_dir = '/data2/tra38/shared_projects/cpe28/CFD/low-res/%s/'

file_name = "p-e_%s_rcp85.nc"

for i in range(9):
    data = NC.Dataset(file_name % (low_res_dirs[i]))
    p_e = np.array(data.variables["p_e"][::])
    lat = np.array(data.variables["lat"][:])
    lon = np.array(data.variables["lon"][:])
    #print (np.amax(p_e))
    print (p_e.shape)   
for i in range(5):
    data = NC.Dataset(file_name % (high_res_dirs[i]))
    p_e = np.array(data.variables["p_e"][::])
    lat = np.array(data.variables["lat"][:])
    lon = np.array(data.variables["lon"][:])
    #print (np.amax(p_e))
    print (p_e.shape)
