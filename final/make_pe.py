import netCDF4 as NC
import numpy as np
import os
import glob
from os import system

CMD_MV = 'mv %s %s'
high_res_dirs = ['BNU', 'CCSM4','GFDL','MIROC','MRI']
low_res_dirs = ['access1-0', 'access1-3', 'cmcc', 'CNRM', 'csiro', 'EC-EARTH', 'FGOALS', 'GISS', 'HADLEY']
#low_res_dirs = ['cmcc', 'CNRM', 'csiro', 'EC-EARTH', 'FGOALS', 'GISS', 'HADLEY']
variables = ['evspsbl', 'evspsblsoi', 'evspsblveg', 'pr']

home_dir = '/data2/tra38/shared_projects/cpe28/CFD/low-res/%s/'

for i in range(9):
    os.chdir(home_dir % (low_res_dirs[i]))
    current_dir = os.getcwd()
    print ("Changed directory to " + str(current_dir) + " working on model: " + str(low_res_dirs[i]))
    try:
        del pr
    except(NameError):
        pass
    try:
        del evspsbl
    except(NameError):
        pass
    try:
        del evspsblsoi
    except(NameError):
        pass
    try:
        del evspsblveg
    except(NameError):
        pass
    
    print ("Old files deleted")
    
    try:    
        pr_files = glob.glob('pr*')
        pr_files.sort()
        pr_data = NC.MFDataset(pr_files)
        pr = np.array(pr_data.variables['pr'][:])
        latitude = np.array(pr_data.variables['lat'][:])
        longitude = np.array(pr_data.variables['lon'][:])
        time_model = np.array(pr_data.variables['time'][:])
        print ("precip data got")
    except(IndexError):
        pass
    try:
        evspsbl_files = glob.glob('evspsbl_*')
        evspsbl_files.sort()
        evspsbl_data = NC.MFDataset(evspsbl_files)
        evspsbl = np.array(evspsbl_data.variables['evspsbl'][:])
        print ("evap data got")
    except(IndexError):
        pass
    try:
        evspsblsoi_files = glob.glob('evspsblsoi_*')
        evspsblsoi_files.sort()
        evspsblsoi_data = NC.MFDataset(evspsblsoi_files)
        evspsblsoi = np.array(evspsblsoi_data.variables['evspsblsoi'][:])
        print ("evap soil got")
    except(IndexError):
        print ("no soil evap")
        pass
    try:
        evspsblveg_files = glob.glob('evspsblveg_*')
        evspsblveg_files.sort()
        evspsblveg_data = NC.MFDataset(evspsblveg_files)
        evspsblveg = np.array(evspsblveg_data.variables['evspsblveg'][:])
        print ("evap canopy got")
    except(IndexError):
        print ("no evap canopy")
        pass
    
    dims = np.shape(evspsbl)
    print ("dims created")
    try:
        evspsbl
    except NameError:
        evspsbl = np.zeros(dims)
        print ("evap set to zeros")
    try:
        evspsblsoi
    except NameError:
        evspsblsoi = np.zeros(dims)
        print ("evap soil set to zeros")
    try:
        evspsblveg
    except NameError:
        evspsblveg = np.zeros(dims)
        print ("evap canopy set to zeros")
    
    EVTRP = evspsbl + evspsblsoi + evspsblveg
    print ("total evap calculated")
    
    P_E = pr - EVTRP
    print ("p-e calculated")
    
    nc_dataset = NC.Dataset('p-e_'+str(low_res_dirs[i])+'_rcp85.nc', 'w', format='NETCDF4_CLASSIC')
    time = nc_dataset.createDimension('time', len(P_E[:,0,0]))
    lat = nc_dataset.createDimension('lat', len(latitude))
    lon = nc_dataset.createDimension('lon', len(longitude))
    times = nc_dataset.createVariable('time', np.float, ('time',))
    lats = nc_dataset.createVariable('lat', np.float, ('lat',))
    lons = nc_dataset.createVariable('lon', np.float, ('lon',))
    p_e_data = nc_dataset.createVariable('p_e', np.float, ('time', 'lat', 'lon'))
    import time
    nc_dataset.history = 'Created ' + time.ctime(time.time())
    lats.units = 'degree_north'
    lons.units = 'degree_east'
    lats[:] = latitude
    lons[:] = longitude
    times[:] = time_model
    p_e_data[:] = P_E
    nc_dataset.close()
    
    system(CMD_MV % (current_dir+'/p-e_*', '/data2/tra38/shared_projects/cpe28/CFD/'))
    
home_dir = '/data2/tra38/shared_projects/cpe28/CFD/high-res/%s/'

for i in range(5):
    os.chdir(home_dir % (high_res_dirs[i]))
    current_dir = os.getcwd()
    print ("Changed directory to " + str(current_dir) + " working on model: " + str(high_res_dirs[i]))
    try:
        del pr
    except(NameError):
        pass
    try:
        del evspsbl
    except(NameError):
        pass
    try:
        del evspsblsoi
    except(NameError):
        pass
    try:
        del evspsblveg
    except(NameError):
        pass
    
    try:
        pr_files = glob.glob('pr*')
        pr_files.sort()
        pr_data = NC.MFDataset(pr_files)
        pr = np.array(pr_data.variables['pr'][:])
        latitude = np.array(pr_data.variables['lat'][:])
        longitude = np.array(pr_data.variables['lon'][:])
        time_model = np.array(pr_data.variables['time'][:])
    except(IndexError):
        pass
    try:
        evspsbl_files = glob.glob('evspsbl_*')
        evspsbl_files.sort()
        evspsbl_data = NC.MFDataset(evspsbl_files)
        evspsbl = np.array(evspsbl_data.variables['evspsbl'][:])
    except(IndexError):
        pass
    
    try:
        evspsblsoi_files = glob.glob('evspsblsoi_*')
        evspsblsoi_files.sort()
        evspsblsoi_data = NC.MFDataset(evspsblsoi_files)
        evspsblsoi = np.array(evspsblsoi_data.variables['evspsblsoi'][:])
    except(IndexError):
        pass
    try:
        evspsblveg_files = glob.glob('evspsblveg_*')
        evspsblveg_files.sort()
        evspsblveg_data = NC.MFDataset(evspsblveg_files)
        evspsblveg = np.array(evspsblveg_data.variables['evspsblveg'][:])
    except(IndexError):
        pass
    
    dims = np.shape(evspsbl)
    print ("dims created")
    try:
        evspsbl
    except NameError:
        evspsbl = np.zeros(dims)
        print ("evap set to zeros")
    try:
        evspsblsoi
    except NameError:
        evspsblsoi = np.zeros(dims)
        print ("evap soil set to zeros")
    try:
        evspsblveg
    except NameError:
        evspsblveg = np.zeros(dims)
        print ("evap canopy set to zeros")
    
    EVTRP = evspsbl + evspsblsoi + evspsblveg
    
    P_E = pr - EVTRP
    
    nc_dataset = NC.Dataset('p-e_'+str(high_res_dirs[i])+'_rcp85.nc', 'w', format='NETCDF4_CLASSIC')
    time = nc_dataset.createDimension('time', len(P_E[:,0,0]))
    lat = nc_dataset.createDimension('lat', len(latitude))
    lon = nc_dataset.createDimension('lon', len(longitude))
    times = nc_dataset.createVariable('time', np.float, ('time',))
    lats = nc_dataset.createVariable('lat', np.float, ('lat',))
    lons = nc_dataset.createVariable('lon', np.float, ('lon',))
    p_e_data = nc_dataset.createVariable('p_e', np.float, ('time', 'lat', 'lon'))
    import time
    nc_dataset.history = 'Created ' + time.ctime(time.time())
    lats.units = 'degree_north'
    lons.units = 'degree_east'
    lats[:] = latitude
    lons[:] = longitude
    times[:] = time_model
    p_e_data[:] = P_E
    nc_dataset.close()
    
    system(CMD_MV % (current_dir+'/p-e_*', '/data2/tra38/shared_projects/cpe28/CFD/'))
