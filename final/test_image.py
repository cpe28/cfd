import numpy as np
import matplotlib.pyplot as plt
plt.switch_backend('agg')
from matplotlib.cm import get_cmap
from matplotlib import cm
import matplotlib.ticker as mticker
import cartopy.mpl.ticker as cticker
from cartopy.feature import NaturalEarthFeature
import cartopy.util
from netCDF4 import Dataset
import netCDF4 as NC
import os
import glob
import moviepy.editor as mpy
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
from shapely.geometry.polygon import LinearRing


data = NC.Dataset("low_res_climo.nc")
low_res = np.array(data.variables["low_res"][::])
lat = np.array(data.variables["lat"][:])
lon = np.array(data.variables["lon"][:])

mm_day_low = (low_res/997.0)*1000.0*86400.


data = NC.Dataset("high_res_climo.nc")
high_res = np.array(data.variables["high_res"][::])
lat_high = np.array(data.variables["lat"][:])
lon_high = np.array(data.variables["lon"][:])

mm_day_high = (high_res/997.0)*1000.0*86400.


annual_low = np.mean(mm_day_low,axis=0)
annual_high = np.mean(mm_day_high,axis=0)
MAM_low = np.mean(mm_day_low[2:5,:,:],axis=0)
MAM_high = np.mean(mm_day_high[2:5,:,:],axis=0)

JJA_low = np.mean(mm_day_low[5:8,:,:],axis=0)
JJA_high = np.mean(mm_day_high[5:8,:,:],axis=0)

SON_low = np.mean(mm_day_low[8:11,:,:],axis=0)
SON_high = np.mean(mm_day_high[8:11,:,:],axis=0)

dec_low = mm_day_low[11,:,:]
dec_high = mm_day_high[11,:,:]
jan_low = mm_day_low[0,:,:]
jan_high = mm_day_high[0,:,:]
feb_low = mm_day_low[1,:,:]
feb_high = mm_day_high[1,:,:]

DJF_low = (dec_low+jan_low+feb_low)/3.0
DJF_high = (dec_high+jan_high+feb_high)/3.0


all_data = np.empty([10,90,180])
all_data[0,:,:] = annual_high
all_data[1,:,:] = annual_low
all_data[2,:,:] = DJF_high
all_data[3,:,:] = DJF_low
all_data[4,:,:] = MAM_high
all_data[5,:,:] = MAM_low
all_data[6,:,:] = JJA_high
all_data[7,:,:] = JJA_low
all_data[8,:,:] = SON_high
all_data[9,:,:] = SON_low

pe_levels = np.arange(-0.5,0.501,0.05)
states_provinces = cfeature.NaturalEarthFeature(category='cultural',name='admin_1_states_provinces_lines',scale='50m',facecolor='none')
fig = plt.figure(figsize=(16,40))
for i in range (1,11,1):
    ax = fig.add_subplot(5,2,i,projection=ccrs.NearsidePerspective(central_longitude=245,central_latitude=45,satellite_height=5000000))
#    ax.set_extent([100,300,-70,70],crs=ccrs.PlateCarree())
    cb = ax.contourf(lon,lat,all_data[(i-1),:,:],pe_levels,transform=ccrs.PlateCarree(),cmap=get_cmap("BrBG"),extend="both")
    ax.coastlines(resolution='50m', linewidth=1.0)
    ax.add_feature(states_provinces, edgecolor='k', linewidth=1.0)
    ax.add_feature(cfeature.BORDERS, edgecolor='k', linewidth=1.0)
    if i == 1:
        ax.set_title("HIGH-r",fontsize=25)
        ax.set_ylabel("Annual",fontsize=25)
    if i == 2:
        ax.set_title("LOW-r",fontsize=25)
#ttl = plt.suptitle("ENSO Development\n",fontsize=30)
#ttl.set_position([0.5,1.00])
cbar_ax = fig.add_axes([0.16, 0.04, 0.7, 0.02])
cbar = fig.colorbar(cb, cax=cbar_ax,ticks=pe_levels[::2],orientation="horizontal")
cbar.set_label(r'P-E $(\frac{mm}{day})$',fontsize=20)
cbar.ax.tick_params(labelsize=20)
plt.subplots_adjust(hspace=0.1)
plt.savefig("test.jpeg", dpi=200,bbox_inches = 'tight')
plt.show()
plt.close()
